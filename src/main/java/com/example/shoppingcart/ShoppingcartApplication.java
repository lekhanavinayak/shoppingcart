package com.example.shoppingcart;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ShoppingcartApplication {

@Bean
public ModelMapper modelMapper() {
    return new ModelMapper();
}

    public static void main(String[] args) {
		SpringApplication.run(ShoppingcartApplication.class, args);
	}

}
