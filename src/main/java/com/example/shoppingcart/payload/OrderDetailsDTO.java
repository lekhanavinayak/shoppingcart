package com.example.shoppingcart.payload;

import lombok.Data;

@Data
public class OrderDetailsDTO {
    private int id;
    private String productQty;
    private String productName;
    private String productPrice;
    private String productImage;
    private String productDescription;
}
