package com.example.shoppingcart.payload;

import com.example.shoppingcart.entity.OrderDetails;
import lombok.Data;

import java.util.List;

@Data
public class UserDetailsDTO {
    int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String addressline1;
    private String addressline2;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private String addressline12;
    private String addressline22;
    private String city2;
    private String state2;
    private String country2;
    private String zipcode2;
    List<OrderDetails> orderDetails;
    String subTotal;
    String grandTotal;
    String totalTax;

}
