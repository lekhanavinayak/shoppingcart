package com.example.shoppingcart.payload;

import lombok.Data;

@Data
public class ProductDetailsDTO {
    int id;
    private String productName;
    private String productPrice;
    private String productQty;
    private String productImage;
    private String productDescription;
}
