package com.example.shoppingcart.repository;

import com.example.shoppingcart.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetialsRepository extends JpaRepository<UserDetails,Integer> {
}
