package com.example.shoppingcart.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class DefaultExceptionHandeler {

    @ExceptionHandler(Exception.class)
    public ModelAndView exceptionHandler(HttpServletRequest req,Exception ex)
    {
        ModelAndView mv=new ModelAndView();
        mv.addObject("exception", ex.getMessage());
        mv.addObject("url", req.getRequestURL());
        mv.setViewName("error");
        return mv;
    }
}
