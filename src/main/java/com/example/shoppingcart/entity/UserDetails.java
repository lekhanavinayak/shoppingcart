package com.example.shoppingcart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String addressline1;
    private String addressline2;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private String addressline12;
    private String addressline22;
    private String city2;
    private String state2;
    private String country2;
    private String zipcode2;

    private String subTotal;
    private String grandTotal;
    private String totalTax;

    @OneToMany(cascade = CascadeType.ALL)
            @JoinColumn(name = "f_id")
    List<OrderDetails> orderDetails;


    public UserDetails(String firstName, String lastName, String email, String phone,
                       String addressline1, String addressline2, String city, String state,
                       String country, String zipcode, String addressline12, String addressline22,
                       String city2, String state2, String country2, String zipcode2
                       ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.addressline1 = addressline1;
        this.addressline2 = addressline2;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zipcode = zipcode;
        this.addressline12 = addressline12;
        this.addressline22 = addressline22;
        this.city2 = city2;
        this.state2 = state2;
        this.country2 = country2;
        this.zipcode2 = zipcode2;

    }


}
