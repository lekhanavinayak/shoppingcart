package com.example.shoppingcart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor

public class ProductDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    private String productName;
    private String productPrice;
    private String productQty;
    private String productImage;
    private String productDescription;

    public ProductDetails(int id, String productName, String productPrice, String productQty, String productImage, String productDescription) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productQty = productQty;
        this.productImage = productImage;
        this.productDescription = productDescription;
    }
}
