package com.example.shoppingcart.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor

public class OrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int u_id;
    int id;
    private String productQty;
    private String productName;
    private String productPrice;
    private String productImage;
    private String productMaxQty;

    private String productDescription;



    @ManyToOne
    UserDetails userDetails;

    public OrderDetails(int id,  String productName,
                        String productPrice, String productImage, String productQty, String productDescription, UserDetails userDetails) {

        this.id = id;
        this.productQty = productQty;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productImage = productImage;
        this.productDescription = productDescription;
        this.userDetails = userDetails;
    }


}
