package com.example.shoppingcart.controller;

import com.example.shoppingcart.entity.OrderDetails;
import com.example.shoppingcart.entity.ProductDetails;
import com.example.shoppingcart.entity.UserDetails;
import com.example.shoppingcart.payload.UserDetailsDTO;
import com.example.shoppingcart.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;
    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping("/")
    public String showProducts(Model model)
    {
       List<ProductDetails> product=productService.getproduct();
       model.addAttribute("productDetails", product);
       return "ProductDetails";
    }

    @RequestMapping("/cart")
    public String cartList(HttpSession httpSession,Model model)
    {
        List<OrderDetails> notes = (List<OrderDetails>) httpSession.getAttribute("NOTE_SESSION");
        model.addAttribute("particularProduct", notes);
        return "shoppingCart";
    }

    @RequestMapping("/addToCart/{id}")
    public String addToCart(@PathVariable(value = "id") int id, Model model, HttpServletRequest request)
    {
        ProductDetails productDetails= productService.getProductDetails(id);

        List<OrderDetails> pd= (List<OrderDetails>) request.getSession().getAttribute("NOTE_SESSION");
        if(pd==null)
        {
            pd=new ArrayList<>();
            request.getSession().setAttribute("NOTE_SESSION",pd);
        }
        OrderDetails od=new OrderDetails(productDetails.getId(),productDetails.getProductName(),
                productDetails.getProductPrice(),productDetails.getProductImage(),productDetails.getProductQty(),
                productDetails.getProductDescription(),null);
        pd.add(od);
        request.getSession().setAttribute("NOTE_SESSION",pd);

        return "redirect:/cart";
    }

    @RequestMapping("/removeFromSession/{id}")
    public String removeFromSession(@PathVariable(value = "id") int id,HttpSession httpSession)
    {
        ProductDetails productDetails= productService.getProductDetails(id);
        OrderDetails od=new OrderDetails(productDetails.getId(),productDetails.getProductName(),
                productDetails.getProductPrice(),productDetails.getProductImage(), productDetails.getProductQty(),

                productDetails.getProductDescription(), null );

        List<OrderDetails> notes = (List<OrderDetails>) httpSession.getAttribute("NOTE_SESSION");
        for(int i=0;i<notes.size();i++)
        {
           if(notes.contains(od))
           {
               notes.remove(od);
           }
        }
        System.out.println(notes);
        return "redirect:/cart";
    }

    @RequestMapping("/userDetails")
    public String userDetails(Model model,@ModelAttribute("userDetails") UserDetailsDTO userDetailsDto)
    {
        System.out.println("_____________");
        model.addAttribute("userDetails",userDetailsDto);
        return "UserDetails";
    }

    @RequestMapping("/saveUserDetails")
    public String saveUserDetails(@ModelAttribute("userDetails")UserDetailsDTO userDetailsDto,HttpServletRequest req, HttpSession httpSession) throws MessagingException {

        List<OrderDetails> notes = (List<OrderDetails>) httpSession.getAttribute("NOTE_SESSION");
        userDetailsDto.setOrderDetails(notes);
        System.out.println("-----"+notes);
        System.out.println("+++++"+userDetailsDto);

        UserDetails userDetails = modelMapper.map(userDetailsDto, UserDetails.class);
        productService.saveUseDetails(userDetails);

        req.getSession().invalidate();

        return "redirect:/";
    }
}
