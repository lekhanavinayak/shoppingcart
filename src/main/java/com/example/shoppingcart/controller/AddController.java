package com.example.shoppingcart.controller;

import com.example.shoppingcart.entity.ProductDetails;
import com.example.shoppingcart.payload.ProductDetailsDTO;
import com.example.shoppingcart.service.AddProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.IOException;

@Controller
public class AddController {

    @Autowired
    AddProductService addProductService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/saveproducts")
    public ResponseEntity<ProductDetailsDTO> saveProducts(@RequestBody ProductDetailsDTO productDetailsDto) throws IOException {

        ProductDetails productDetailsRequest = modelMapper.map(productDetailsDto, ProductDetails.class);
        ProductDetails productDetails = addProductService.save(productDetailsRequest);
        ProductDetailsDTO productDetailsDTO = modelMapper.map(productDetails, ProductDetailsDTO.class);
        return ResponseEntity.ok().body(productDetailsDTO);
    }
}
