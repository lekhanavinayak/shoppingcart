package com.example.shoppingcart.service;

import com.example.shoppingcart.entity.OrderDetails;
import com.example.shoppingcart.entity.UserDetails;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import javax.mail.*;


@Service
public class EmailSenderService {

    private final TemplateEngine templateEngine;

    private final JavaMailSender javaMailSender;

    public EmailSenderService(TemplateEngine templateEngine, JavaMailSender javaMailSender) {
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
    }

    public String sendMail(UserDetails userDetails) throws MessagingException {

        javax.mail.internet.MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        helper.setSubject("Order Confirmation Mail " );
        helper.setText(buildText(userDetails), true);
        helper.setTo(userDetails.getEmail());
        javaMailSender.send(mimeMessage);
        return "Sent";
    }

    private String buildText(UserDetails userDetails) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<h3>Hi " + userDetails.getFirstName() +", Your Order Details</h3>");
        sb.append("</head>");
        sb.append("<table>");
        sb.append("<th> Product Name </th>");
        sb.append("<th> Prize </th>");
        sb.append("<th> Total Tax </th>");
        sb.append("<th> Total Amount </th>");


        for (OrderDetails orderDetails : userDetails.getOrderDetails()) {
            sb.append("<tr>");
            sb.append("<td> " + orderDetails.getProductName() + " </td>");
            sb.append("<td> " + orderDetails.getProductPrice() + " </td>");
            sb.append("<td> " + userDetails.getTotalTax() + " </td>");
            sb.append("<td> " + userDetails.getSubTotal() + " </td>");
            sb.append("</tr>");
        }
        sb.append("</table>");
        sb.append("<h3>Thanks for Shopping with us. </h3>");
        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }

}

