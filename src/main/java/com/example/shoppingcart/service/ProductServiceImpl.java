package com.example.shoppingcart.service;

import com.example.shoppingcart.entity.ProductDetails;
import com.example.shoppingcart.entity.UserDetails;
import com.example.shoppingcart.repository.ProductDetailsRepository;
import com.example.shoppingcart.repository.UserDetialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDetailsRepository productDetailsRepository;
    @Autowired
    UserDetialsRepository userDetialsRepository;
    @Autowired
    private EmailSenderService emailSenderService;

    @Override
    public List<ProductDetails> getproduct() {
       return productDetailsRepository.findAll();
    }

    @Override
    public ProductDetails getProductDetails(int id) {
        Optional< ProductDetails > optional = productDetailsRepository.findById(id);
        ProductDetails productDetails = null;
        if (optional.isPresent()) {
            productDetails = optional.get();
        } else {
            throw new RuntimeException(" Product Details not found for id : " + id);
        }
        return productDetails;
    }

    @Override
    public UserDetails saveUseDetails(UserDetails userDetails) throws MessagingException {

        emailSenderService.sendMail(userDetails);
       return userDetialsRepository.save(userDetails);

    }

}
