package com.example.shoppingcart.service;

import com.example.shoppingcart.entity.ProductDetails;
import com.example.shoppingcart.repository.ProductDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddProductServiceImpl implements AddProductService {

    @Autowired
    ProductDetailsRepository productDetailsRepository;

    @Override
    public ProductDetails save(ProductDetails productDetails) {
       return productDetailsRepository.save(productDetails);
    }
}
