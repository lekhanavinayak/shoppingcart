package com.example.shoppingcart.service;

import com.example.shoppingcart.entity.ProductDetails;
import com.example.shoppingcart.entity.UserDetails;

import javax.mail.MessagingException;
import java.util.List;

public interface ProductService {
    List<ProductDetails> getproduct();

    public ProductDetails getProductDetails(int id);

    public UserDetails saveUseDetails(UserDetails userDetails) throws MessagingException;
}
