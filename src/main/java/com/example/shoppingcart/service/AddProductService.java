package com.example.shoppingcart.service;

import com.example.shoppingcart.entity.ProductDetails;

public interface AddProductService {

    ProductDetails save(ProductDetails productDetails);
}
