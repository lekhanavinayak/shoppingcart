package com.example.shoppingcart.shoppingTest;

import com.example.shoppingcart.entity.ProductDetails;
import com.example.shoppingcart.entity.UserDetails;
import com.example.shoppingcart.repository.ProductDetailsRepository;
import com.example.shoppingcart.repository.UserDetialsRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


import java.io.IOException;

@SpringBootTest
@ActiveProfiles("test")
public class ShoppingTest {

    @Autowired
    ProductDetailsRepository productDetailsRepository;
    @Autowired
    UserDetialsRepository userDetialsRepository;

    @Test
    public void saveUserDetailsTest() throws IOException {
        UserDetails userDetails = (new UserDetails("Lekhana", "bhat", "lekhana@gmail.com",
                "9876543234", "brn", "ben", "bll", "kar", "ind", "546", "brn", "ben", "bll", "kar", "ind", "546"));

        UserDetails u = userDetialsRepository.save(userDetails);
        Assertions.assertThat(u.getZipcode()).isEqualTo("546");
    }

    @Test
    public void saveProductsTest() throws IOException {
        ProductDetails productDetails = (new ProductDetails(1, "iphone", "45000", "2", "phone","phone"));
        ProductDetails p = productDetailsRepository.save(productDetails);
        Assertions.assertThat(p.getProductPrice().isEmpty());
    }

    @Test
    public void userDetailsTest() throws IOException {
        UserDetails userDetails = (new UserDetails("Lekhana", "bhat", "lekhana@gmail.com",
                "9876543234", "brn", "ben", "gokarna", "karnataka", "ind", "546", "brn", "ben", "bll", "kar", "ind", "546"));

        UserDetails u = userDetialsRepository.save(userDetails);
        Assertions.assertThat(u.getZipcode()).isEqualTo("546");

    }

    @Test
    public void addToCartTest() throws IOException {
        int id = 1;
        ProductDetails productDetails =(new ProductDetails(1, "iphone", "45000", "2", "phone","phone"));
        ProductDetails p = productDetailsRepository.save(productDetails);
        Assertions.assertThat(p.getProductPrice().isEmpty());
    }

    @Test
    public void addProductTest() throws IOException {
        ProductDetails productDetails = (new ProductDetails(1, "iphone", "45000", "2", "phone","phone"));
        ProductDetails p = productDetailsRepository.save(productDetails);
        Assertions.assertThat(p.getProductPrice().isEmpty());
    }
    }

