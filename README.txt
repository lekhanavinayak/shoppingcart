This project is simple shopping cart where user can add products available.
Once the products are added and user details are given,
mail will be sent to the given email id.


Endpoints available in this project.

1./: home page
2./cart: It will return with initial prodcut/s added in the cart. 
3./addToCart/{id}: It will store the product information into database.
4./userDetails: It will return user details needed to be added.
5./saveUserDetails: It will save user details given by the user
and also sends the mail to given email about the order details
6./removeFromSession/{id}: to remove item from the cart.
7./saveproducts: to add available products in the database.


How to run the application:


1.By using Postman, Update the database with product details. 
2.After updating the database run the application by using http://localhost:8085/,
now you can access the home page with the product details.
3.By clicking on "Add to cart" you can add product to the cart
and it will be directed to the cart page and
there you need to mention the quantity and click on to the "Update cart" to know the tax, subtotal, grandtotal.
4.By clicking on "Continue shopping" it will be redicted to the home page and you can add more products to the cart.
5.By clicking on "Next" it will direct to the "User detail" page.
6.In User detail page you need to provide the valid Email Id,Phone number and other details
and click on save button.
7.And after clicking on save button Order confirmation mail will be sent to the provided Email Id.


To send mail, GMAIL SMTP is used.
To store data in the database, MYSQL is used.